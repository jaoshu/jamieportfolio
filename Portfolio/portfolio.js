// JavaScript Document
// JavaScript Document

//constructor
class Books {
	constructor("authLast", "authFirst", "authLast2", "authFirst2", "authLast3", "authFirst3",
				"authLast4", "authFirst4", "title", "edition", "subject", "description"){
		this.authLast = authLast;
		this.authFirst = authFirst;
		this.authLast2 = authLast2;
		this.authFirst2 = authFirst2;
		this.authLast3 = authLast3;
		this.authFirst3 = authFirst3;
		this.authLast4 = authLast4;
		this.authFirst4 = authFirst4;
		this.title = title;
		this.edition = edition;
		this.subject = subject;
		this.description = description;
	}
	
//instance
var books = [];

//adding
//1
books.push("Edwards", "Richard", "", "", "", "", "", "", "Competitive Debate", "", "Science",
		  "The German 'kritik' has done much damage to Western philosophy and thinking.");
//2
books.push("Sokal", "Alan", "Bricmont", "Jean", "", "", "", "","Fashionable Nonsense: Postmodern Intellectuals' Abuse of Science", "", "Science", "Police investigations represent one of the closest daily, formal scientific experiments in society. Probable cause statements rest upon the sturdy legs of qualitative and quantitative facts. In one example, a case of the judge, police officer, and professor, the question involving whether a piece of evidence was sent remains a question of material truth. Either the policeman sent the file or he didn't. Whether the file was lost in transit, remains a separate question of fact. It is simply not reasonable for the expert witness to attest that 'both are telling their truth.'");
//3
books.push("Weston", "Anthony", "", "", "", "", "", "","A Rulebook for Arguments", "Third Edition", "Philosophy", "Refrain from employing classical fallacies in argument.");
//4
books.push("VanderMey", "", "Meyer", "", "Van Rys", "", "Sebranek", "", "The College Writer: A Guide to Thinking, Writing, and Researching", "Fifth Edition", "Reasoning", "Learn to deduce a topic into a workable thesis, support a claim with directly relevant evidence, form syllogisms, prepare for writing with pre-writing strategies, and address contemporary problems using formal exegeses and essays.");
//5
books.push("Skousen," "Cleon", "", "", "", "", "", "", "The 5000 Year Leap: A Miracle that Changed the World", "", "US Constitutional History and Political Philosophy", "The Founders, despite all imperfections, studied those Grecian, Roman, and Hebrew philosophers, prayed and sacrificed to devolve upon the most suitable form of free government, and received inspiration through prayer for the successful and miraculous ratification of our founding document and the supreme law of the land.");
//6
books.push("Scalia", "Antonin", "Garner", "Bryan", "", "", "", "", "Reading Law: The Interpreting of Legal Texts", "","US Law and Philosophy", "The Constitution, contrary to our progressive brethren's strident pleadings, is not a living document. The Founders prescribed specific meanings when writing the inspired document. Thus, a correct study of context proves essential for judges sworn to uphold and defend the original meanings of the US Constitution.");
books.push("Boghossian", "Paul", "", "", "", "", "", "", "Fear of Knowledge: Against Relativism and Constructivism", "", "The Science of Reasoning", "Truth exists independent of human thought and opinion.");
books.push("Steyn", "Mark", "", "", "", "", "", "", "Mark Steyn Confounds Epistemic and Cultural Relativism", "[youtube link]", "Material Facts", "Relativism proves itself as the slipperiest of -isms because it says that if all value systems have the same power to prosper people, we effectually tell the world we have no core values. Specifically, one draws the inference that the British system of thought, which originates from the Hebrew tradition, remains superlative at producing human peace, prosperity, and happiness.");
books.push("Rectenwald", "Michael", "", "", "", "", "", "", "Springtime for Snowflakes: Social Justice and its Postmodern Parentage, An Academic's Memoir", "", "Pitting Epistemic Relativism or Marxism contra an Epistemology of Material Facts", "Michael Rectenwald takes readers down the dark path he followed to become a temporarily convinced Marxist before coming to terms with objective reality after suffering persecution at the hands of those he once deemed his compassionate brethren.");

//I want to output the list of objects in the HTML file
//so I thought it could be done this way?
//On the other hand, console.log() just outputs it to the console,
//which we can still use for debugging
books.getElementsByTagName('td');
for(var i = 0, li; li = li[i]; i++){
	console.log(books);
}

//Another possibility for outputting the data to the HTML table
//From the Javascript Ninja text
function getNodes(htmlString, doc, fragment){
	const map = {
		"<td":[3,"<table><body><tr>","</tr></body></table>"],
		"<th":[3,"<table><body><tr>","</tr></body></table>"],
		"<tr":[2,"<table><th>","</th></table>"]
	};
	const tagName = htmlString.match(/<\w+/);
	let mapEntry = tagName ? map[tagName[0]]:null;
	if(!mapEntry){
		mapEntry = [0," ", " "];
	}
	let div = (doc || document).createElement("div");
	div.innerHTML = mapEntry[1] + htmlString + mapEntry[2];
	while(mapEntry[0]--){
		div=div.lastChild;
	}
	if(fragment){
		while(div.firstChild){
			fragment.appendChild(div.firstChild);
		}
		return div.childNodes;
	}
}

document.addEventListener("DOMContentLoaded",()=>{
	function insert(elems, args, callback){
		if(elems.length){
			const doc = elems[0].ownerDocument || elems[0],
				  fragment = doc.createDocumentFragment(),
				  scripts = getNodes(args, doc, fragment),
				  first = ragment.firstChild;
			if(first){
				for(let i = 0; elems[i]; i++){
					callback.call(root(elems[i],first),
								 i > 0 ? fragment.cloneNode(true):fragment);
				}
			}
		}
	}
	const divs = document.querySelectorAll("div");
	insert(divs,"<b>Name:</b>",function(fragment){
		this.appendChild(fragment);
	});
	insert(divs, "<span>First</span><span>Last</span>",
		  function(fragment){
		this.parentNode.insertBefore(fragment, this);
	});
});

}
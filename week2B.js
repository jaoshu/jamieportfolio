// JavaScript Document 
/*Global console*/ 

const calculator = { 
///Nesting functions into a single object called calculator. 

	readText() { 
		//This block creates memory space for two variables and retrieves the input
		//Ideally, should apply as a scanner to all the following functions
		let inputValue = document.getElementById("inputTest").value; 
		let getDiv = document.getElementById("div1"); 
		getDiv.innerHTML = inputValue; 
	}, 

	addNumbers() { 

		let inputValue = document.getElementById("inputNumber").value; 
		let getDiv = document.getElementById("div1"); 

		if (!isNaN(inputValue)) {
	
 			alert("Must input numbers");
    		return false;
	
		else {
	
			addInputNumbers(Number(inputValue)); 
			getDiv.innerHTML = runningTotal; 
		
			} 
		}
	}, 

	//Grabs a potentially infinite set of input numbers and adds them
	addInputNumbers(value) { 
		runningTotal = 0; 
	
		for (i = 0; i < value; i++) { 
			runningTotal += i; 
		} 
			runningTotal += Number(value); 
			return runningTotal; 
	}, 

	addSubsequentNumbers() { 
		let numberOne = document.getElementById("addThem").value; 
		let numberTwo = document.getElementById("addThem2").value; 
		
		if (!isNaN(numberOne) && !isNaN(numberTwo)) { 
			let total = Number(numberOne) + Number(numberTwo); 
			let getDiv = document.getElementById("div1"); 
			getDiv.innerHTML = total; 
		} 
	}, 

	multiply() { 
		let numberOneM = document.getElementById("firstOne").value; 
		let numberTwoM = document.getElementById("secondOne").value; 

		if (!isNaN(numberOneM) && !isNaN(numberTwoM)) { 
			document.getElementById("result").innerHTML = Number(numberOneM) * Number(numberTwoM); 
		} 
	}, 

	divide() { 
		let numberOneD = document.getElementById("firstOne").value; 
		let numberTwoD = document.getElementById("secondOne").value; 

		if (!isNaN(numberOneD) && !isNaN(numberTwoD)) { 
			document.getElementById("result").innerHTML = Number(numberOneD) / Number(numberTwoD); 
		} 
	} 

}; 
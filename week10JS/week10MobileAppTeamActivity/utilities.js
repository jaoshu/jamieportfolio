// JavaScript Document
export function getJSON(url){
	return fetch(url)
		.then((response)=>{
			if(!response.ok){
				throw Error(response.statusText);
			} else {
				return response.json();
			}
		}
			  )
		.catch((error)=>{
			console.log(error);
		}
			   );
}

getJSON('https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2019-01-01&endtime=2019-02-02');
		
export function getLocation(options){
	return new Promise(function(resolve,reject){//A promise represents a distant completion of an asynchronous operation in Javascript
		navigator.geolocation.getCurrentPosition(resolve, reject, options);
	});
	
};
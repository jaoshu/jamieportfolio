// JavaScript Document
//https://stackoverflow.com/questions/957537/how-can-i-display-a-javascript-object
str = JSON.stringify(obj);//Supplant "obj" with your object
str = JSON.stringify(obj, null, 4);//Format the output with this
console.log(str);//Output to dev tools in the browser
alert(str);//Display output using window.alert();

//https://www.w3schools.com/js/js_output.asp
document.getElementById("text").innerHTML;

//scotch.io/tutorials/how-to-use-the-javascript-fetch-api-to-get-data
//Get the list
const ul = document.getElementById('books');
//Get random elements from the list
const url = 'https://whatever.com/api/?results=10;'  